﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public string menu_lvl;

    public void ContinueGame()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }

    public void Menu_lvl()
    {
        SceneManager.LoadScene(menu_lvl);
    }
}
