﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    //public GameMaster gameMaster;

    private void Start()
    {
        //gameMaster = GameObject.Find("GameMaster");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        gameObject.SetActive(false);
        //gameMaster.GetComponent<GameMaster>().PlusScore();
        //gameMaster.PlusScore();
        transform.parent.gameObject.GetComponent<Target_spawn>().SpawnRandom();
    }
}
