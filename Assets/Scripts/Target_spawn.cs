﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target_spawn : MonoBehaviour
{
    public GameObject target;
    public List<GameObject> target_spawn_points;
    private int last_spawned_ind = 0;
    private GameMaster gameMaster;

    private float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        gameMaster = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        foreach (GameObject obj in target_spawn_points)
        {
            obj.active = false;
            //obj.GetComponent<Target>().gameMaster = GameMaster.GetComponent<GameMaster>();
        }
        target_spawn_points[0].active = true;
    }

    private void Update()
    {
        timer += Time.deltaTime;
    }

    public void SpawnRandom()
    {
        if (timer < 0.2f)
            return;
        timer = 0;
        gameMaster.PlusScore();
        int random_num = Random.RandomRange(0, target_spawn_points.Count);
        while(random_num == last_spawned_ind)
            random_num = Random.RandomRange(0, target_spawn_points.Count);
        target_spawn_points[random_num].active = true;
        last_spawned_ind = random_num;
        CheckCorrect();
        print("spawning random");
    }

    void CheckCorrect()
    {
        int a = 0;
        foreach(GameObject obj in target_spawn_points)
        {
            if (obj.activeSelf)
                a++;
            if (a > 1)
            {
                obj.SetActive(false);
                print("delete");
            }
        }

    }
}
