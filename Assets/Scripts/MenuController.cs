﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public string game_lvl;
    public void Play_lvl()
    {
        SceneManager.LoadScene(game_lvl);
    }

    public void ExitApplication()
    {
        Application.Quit();
    }
}
