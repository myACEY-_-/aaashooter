﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon_script : MonoBehaviour
{
    public float offset = 0;
    public GameObject ammo;
    public List<Transform> shootDirs;

    private float timeShoot;
    public float startTime;

    public int maxAmmo;
    [SerializeField]
    private int curAmmo;
    public float reloadTime;
    [SerializeField]
    private float curReloadTime;
    [SerializeField]
    public bool reloading = false;

    public AudioClip shoot_s;
    private AudioSource shoot_source;
    public AudioClip reload_s;
    private AudioSource reload_source;

    public Text ammo_text;
    public bool automatic = false;

    private SpriteRenderer spRender;

    private void Start()
    {
        /*curAmmo = maxAmmo;
        shoot_source = gameObject.AddComponent<AudioSource>();
        shoot_source.playOnAwake = false;
        shoot_source.clip = shoot_s;
        reload_source = gameObject.AddComponent<AudioSource>();
        reload_source.playOnAwake = false;
        reload_source.clip = reload_s;
        ammo_text.text = string.Format("{0} {1}.{2}", gameObject.name, curAmmo, maxAmmo);

        spRender = GetComponent<SpriteRenderer>();
        print("Start");*/
    }

    public void LoadAll()
    {
        curAmmo = maxAmmo;
        shoot_source = gameObject.AddComponent<AudioSource>();
        shoot_source.playOnAwake = false;
        shoot_source.clip = shoot_s;
        reload_source = gameObject.AddComponent<AudioSource>();
        reload_source.playOnAwake = false;
        reload_source.clip = reload_s;
        ammo_text.text = string.Format("{0} {1}.{2}", gameObject.name, curAmmo, maxAmmo);

        spRender = GetComponent<SpriteRenderer>();
        print("Start");
    }
    public void UpdateUI()
    {
        ammo_text.text = string.Format("{0} {1}.{2}", gameObject.name, curAmmo, maxAmmo);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))// && curAmmo != maxAmmo && !reloading)
        {
            print('a');
            curReloadTime = reloadTime;
            reloading = true;
            reload_source.Play();
        }

        if (transform.eulerAngles.z < 90 || transform.eulerAngles.z > 270)
            spRender.flipY = false;
        else
            spRender.flipY = true;

        if (timeShoot <= 0 && curAmmo > 0 && !reloading)
        {
            if (Input.GetMouseButtonDown(0) || (automatic && Input.GetMouseButton(0)))
            {
                //print("pif");
                foreach(Transform i in shootDirs)
                    Instantiate(ammo, i.position, i.rotation);
                timeShoot = startTime;
                shoot_source.Play();
                curAmmo--;
                ammo_text.text = string.Format("{0} {1}.{2}", gameObject.name, curAmmo, maxAmmo);
                if (curAmmo == 0)
                {
                    reloading = true;
                    curReloadTime = reloadTime;
                    reload_source.Play();
                }
            }
        }
        else
        {
            timeShoot -= Time.deltaTime;
            curReloadTime -= Time.deltaTime;
            if (reloading && curReloadTime <= 0)
            {
                curAmmo = maxAmmo;
                reloading = false;
                ammo_text.text = string.Format("{0} {1}.{2}", gameObject.name, curAmmo, maxAmmo);
            }
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float rotateZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotateZ + offset);
    }

}
