﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_controller : MonoBehaviour
{
    public float speed;
    private Vector2 direction;
    private Rigidbody2D rb;

    public List<GameObject> guns;
    public int curGunIndex = 0;
    private GameObject weaponHolder;

    public AudioClip stepsSound;
    private AudioSource stepsSource;

    private bool stop_footsteps;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        weaponHolder = transform.Find("WeaponHolder").gameObject;
        foreach(Transform obj in weaponHolder.transform)
        {
            guns.Add(obj.gameObject);
            obj.gameObject.GetComponent<Weapon_script>().LoadAll();
            obj.gameObject.SetActive(false);
        }
        guns[curGunIndex].SetActive(true);
        guns[curGunIndex].GetComponent<Weapon_script>().UpdateUI();
        print("a");
        stepsSource = gameObject.AddComponent<AudioSource>();
        stepsSource.playOnAwake = false;
        //stepsSource.loop = true;
        stepsSource.clip = stepsSound;
    }

    // Update is called once per frame
    void Update()
    {
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            guns[curGunIndex].SetActive(false);
            curGunIndex = (curGunIndex + 1) % guns.Count;
            guns[curGunIndex].SetActive(true);
            guns[curGunIndex].GetComponent<Weapon_script>().UpdateUI();
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            guns[curGunIndex].SetActive(false);
            curGunIndex -= 1;
            if(curGunIndex == -1)
                curGunIndex = guns.Count - 1;
            guns[curGunIndex].SetActive(true);
            guns[curGunIndex].GetComponent<Weapon_script>().UpdateUI();
        }
    }
    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + direction * speed * Time.fixedDeltaTime);
        if (direction.x != 0 || direction.y != 0)
        {
            if (!stop_footsteps)
            {
                StartCoroutine(playFootsteps());
            }
        }
    }

    IEnumerator playFootsteps()
    {
        stop_footsteps = true;
        stepsSource.PlayOneShot(stepsSound);
        yield return new WaitForSeconds(1f);
        stop_footsteps = false;
    }
}
