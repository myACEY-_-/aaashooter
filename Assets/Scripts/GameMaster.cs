﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    public float timeVal = 90;
    public Text timerTxt;
    [SerializeField]
    private int score = 0;
    public Text scoreTxt;

    [SerializeField]
    private bool game_paused = false;

    public GameObject pauseMenu;

    public bool show_hint = true; //show at start?
    public GameObject startingHint;
    public Text startingHintTxt;
    private bool showingHint = true; //do i show hint right now?
    [SerializeField]
    private float hintTimer = 10f;

    public string menuScene;


    public GameObject endMenu;
    public Text scoreEndTxt;

    private void Start()
    {
        Time.timeScale = 1;
        game_paused = false;
        AudioListener.pause = false;
        if (!show_hint)
            return;
        AudioListener.pause = true;
        startingHint.SetActive(true);
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (showingHint && show_hint)
        {
            hintTimer -= Time.unscaledDeltaTime;
            DisplayHintTimer(hintTimer);
            if (hintTimer <= 0)
            {
                showingHint = false;
                startingHint.SetActive(false);
                Time.timeScale = 1;

                AudioListener.pause = false;
            }
            return;
        }
        #region timer
        if (timeVal > 0)
            timeVal -= Time.deltaTime;
        else
        {
            timeVal = 0;
            EndOfGame();
        }
        //timer.text = string.Format("{0}", timeVal);
        DisplayTime(timeVal);
        #endregion

        #region pause
        if (!game_paused && Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0;
            game_paused = true;
            AudioListener.pause = true;
            pauseMenu.SetActive(true);
        }
        else if (game_paused && Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1;
            game_paused = false;
            AudioListener.pause = false;
            pauseMenu.SetActive(false);
        }
        #endregion

    }

    void DisplayTime(float timeToDisplay)
    {
        if (timeToDisplay < 0)
            timeToDisplay = 0;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60f);
        int seconds = Mathf.FloorToInt(timeToDisplay % 60);
        timerTxt.text = string.Format("{0}.{1}", minutes.ToString("00"), seconds.ToString("00"));
    }

    void EndOfGame()
    {
        scoreEndTxt.text = string.Format("{0}", score);
        endMenu.SetActive(true);
        Time.timeScale = 0;
    }

    void DisplayHintTimer(float timeToDisplay)
    {
        startingHintTxt.text = string.Format("{0}", Mathf.FloorToInt(timeToDisplay));
    }

    public void PlusScore()
    {
        score++;
        scoreTxt.text = string.Format("{0}", score.ToString("000"));
        game_paused = false;
        AudioListener.pause = false;
    }

    #region some shit stuff, ui...

    public void ContinueGameBtn()
    {
        Time.timeScale = 1;
        game_paused = false;
        AudioListener.pause = false;
        pauseMenu.SetActive(false);
    }

    public void ExitMainMenu()
    {
        SceneManager.LoadScene(menuScene);
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    #endregion
}